package com.tphportal.constant;

public class Constants {
	public static final String awsAccessKey = System.getenv("accessKey");;
	public static final String awsSecretKey = System.getenv("secretKey");;
	public static final String TPH_LAMBDA = "tphdashboard";
}
