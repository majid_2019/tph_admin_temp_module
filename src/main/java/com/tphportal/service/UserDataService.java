package com.tphportal.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.tph.util.AWSClientBuilder;
import com.tphportal.constant.Constants;
import com.tphportal.model.RegistraionRequestModel;
import com.tphportal.model.UsersDataModel;

public class UserDataService {

	public void insertUserData(List<UsersDataModel> userDataList) {
		for (UsersDataModel usersDataModel : userDataList) {
			
			RegistraionRequestModel registraionRequestModel = new RegistraionRequestModel();
			registraionRequestModel.setCompanyName(usersDataModel.getCompanyName());
			registraionRequestModel.setGstNumber(usersDataModel.getGstNo());
			registraionRequestModel.setMobileNumber(usersDataModel.getUserMobile());
			registraionRequestModel.setEmail(usersDataModel.getEmail());
			registraionRequestModel.setAddress(usersDataModel.getAddressOne());
			registraionRequestModel.setCountryId(Integer.parseInt(usersDataModel.getCountryId()));
			registraionRequestModel.setStateId(Integer.parseInt(usersDataModel.getStateId()));
			registraionRequestModel.setCityId(Integer.parseInt(usersDataModel.getCityId()));
			registraionRequestModel.setPassword(usersDataModel.getPassword());
			registraionRequestModel.setContactPerson(usersDataModel.getFirstName());
			registraionRequestModel.setUserName(usersDataModel.getUserName());
			registraionRequestModel.setPinCode(usersDataModel.getPinCode()!=null?Integer.parseInt(usersDataModel.getPinCode()):null);
			registraionRequestModel.setIpAddress("103.115.202.77");
			
			if(usersDataModel.getIsSeller()!=null && usersDataModel.getIsSeller().equalsIgnoreCase("Yes"))
			registraionRequestModel.setIsSeller(true);
			if(usersDataModel.getIsBuyer()!=null && usersDataModel.getIsBuyer().equalsIgnoreCase("Yes"))
				registraionRequestModel.setIsBuyer(true);
			
			Map<String, Object> mapRequest = new HashMap<String, Object>();
			mapRequest.put("payLoad", new Gson().toJson(registraionRequestModel));
			mapRequest.put("requestId", "sellerRegistration");
			
			String myRequest = new Gson().toJson(mapRequest);
			System.out.println("Lambda Function -- "+Constants.TPH_LAMBDA);
			String resBody = AWSClientBuilder.invokeLambdaFunction(Constants.TPH_LAMBDA, new Gson().toJson(mapRequest));
			System.out.println("--Final Res -- "+resBody);
			
		}
	}

}
