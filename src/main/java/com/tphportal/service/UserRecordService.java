package com.tphportal.service;

import java.sql.Connection;
import java.sql.SQLException;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.tph.jooq.tph_db.tables.Users;
import com.tph.util.DBConnection;

public class UserRecordService {
	public Integer fetchUserRecord() {
		try (Connection connection = DBConnection.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {

			Integer d = dslContext.selectCount().from(Users.USERS).fetchOne(0, Integer.class);
			System.out.println("--- " + d);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) {
		new UserRecordService().fetchUserRecord();
	}
}
