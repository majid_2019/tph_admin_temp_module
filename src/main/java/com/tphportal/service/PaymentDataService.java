package com.tphportal.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import com.google.gson.Gson;
import com.tph.jooq.tph_db.tables.Users;
import com.tph.util.AWSClientBuilder;
import com.tph.util.DBConnection;
import com.tphportal.constant.Constants;
import com.tphportal.model.PaymentDetailRequestModel;

public class PaymentDataService {

	public void insertUserData(List<PaymentDetailRequestModel> userDataList) {
		try (Connection connection = DBConnection.getConnection();
				DSLContext dslContext = DSL.using(connection, SQLDialect.MYSQL)) {
			
			for (PaymentDetailRequestModel paymentRequest : userDataList) {
				
				Integer selletId = getSellerId(paymentRequest.getMobileNumber(), dslContext);
				
				System.out.println("Seller sellerId-- "+selletId);
				
				paymentRequest.setSellerID(selletId);
				
				System.out.println("-Final Request -- "+new Gson().toJson(paymentRequest));
				
				Map<String, Object> mapRequest = new HashMap<String, Object>();
				mapRequest.put("payLoad", new Gson().toJson(paymentRequest));
				mapRequest.put("requestId", "savePayment");
				
				String myRequest = new Gson().toJson(mapRequest);
				String resBody = AWSClientBuilder.invokeLambdaFunction(Constants.TPH_LAMBDA, myRequest);
				System.out.println("--Final Res -- "+resBody);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Integer getSellerId(String mobileNumber, DSLContext dslContext) {
		System.out.println("Seller mobile number-- "+mobileNumber);
		Record record = dslContext.selectFrom(Users.USERS).where(Users.USERS.USER_MOBILE.eq(mobileNumber))
		.and(Users.USERS.USERNAME.eq(mobileNumber)).orderBy(Users.USERS.ID.desc()).fetchOne();
		if(record!=null) {
			return record.get(Users.USERS.ID);
		}
		return null;
	}

}
