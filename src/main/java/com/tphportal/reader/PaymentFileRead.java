package com.tphportal.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.tphportal.model.PaymentDetailRequestModel;
import com.tphportal.service.PaymentDataService;

public class PaymentFileRead {
	public List<PaymentDetailRequestModel> readFile(InputStream objectData) {
		List<PaymentDetailRequestModel> usersList = new ArrayList<PaymentDetailRequestModel>();
		System.out.println("Inside readFile");

		// Finds the workbook instance for XLSX file
		try (XSSFWorkbook myWorkBook = new XSSFWorkbook(objectData);) {
			PaymentDetailRequestModel paymentDetail = null;

			System.out.println("Inside objectData");

			// Return first sheet from the XLSX workbook
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);

			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = mySheet.iterator();

			// Traversing over each row of XLSX file
			int rowCounter = 0;
			Boolean isStopReading = false;

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();

				paymentDetail = new PaymentDetailRequestModel();
				if (rowCounter > 2 && !isStopReading) {
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();

						switch (cell.getColumnIndex()) {
						case 0:
							System.out.println("Case-- 0-- " + getCellValue(cell));
							if (getCellValue(cell) == null) {
								// rowCounter = -2;
								isStopReading = true;
							} else {
								Object mobileNumber = new BigDecimal(
										getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								paymentDetail.setProductId(String.valueOf(mobileNumber));
							}
							break;
						case 1:
							System.out.println("Case-- 1-- " + getCellValue(cell));
							if (!isStopReading) {
								Object password = new BigDecimal(
										getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								paymentDetail.setMobileNumber(String.valueOf(password));
							}

							break;

						case 3:
							System.out.println("Case-- 3-- " + getCellValue(cell));

							if (!isStopReading && getCellValue(cell) != null) {
								Object dashBoardAmount = new BigDecimal(
										getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								paymentDetail.setDashboardAmount(new BigDecimal(String.valueOf(dashBoardAmount)));
							}
							break;

						case 4:
							System.out.println("Case-- 4-- " + getCellValue(cell));
							if (!isStopReading && getCellValue(cell) != null) {
								Object tphBaseAmount = new BigDecimal(
										getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								paymentDetail.setTphBaseAmount(new BigDecimal(String.valueOf(tphBaseAmount)));
							}

							break;

						case 5:
							System.out.println("Case-- 5-- " + getCellValue(cell));
							if (!isStopReading && getCellValue(cell) != null) {
								Object cgst = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", ""))
										.toBigInteger();
								paymentDetail.setCgst(new BigDecimal(String.valueOf(cgst)));
							}

							break;

						case 6:
							System.out.println("Case-- 6-- " + getCellValue(cell));
							if (!isStopReading && getCellValue(cell) != null) {
								Object sgst = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", ""))
										.toBigInteger();
								paymentDetail.setSgst(new BigDecimal(String.valueOf(sgst)));
							}

							break;

						case 7:
							System.out.println("Case-- 7-- " + getCellValue(cell));
							if (!isStopReading && getCellValue(cell) != null) {
								Object igst = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", ""))
										.toBigInteger();
								paymentDetail.setIgst(new BigDecimal(String.valueOf(igst)));
							}
							break;

						case 8:
							System.out.println("Case-- 8-- " + getCellValue(cell));
							if (!isStopReading && getCellValue(cell) != null) {
								Object totalAmount = new BigDecimal(
										getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								paymentDetail.setTphTotalAmount(new BigDecimal(String.valueOf(totalAmount)));
							}
							break;
						}
					}
					if (!isStopReading) {
						paymentDetail.setIsPaymentSuccess(true);
						paymentDetail.setPayType("PORT");
						paymentDetail.setPaymentStatus("Success");
						paymentDetail.setReceiptNumber(String.valueOf(new Date().getTime()));
						paymentDetail.setTransactionId(String.valueOf(new Date().getTime()));
						usersList.add(paymentDetail);
					}

				}
				rowCounter++;

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("usersList -- " + new Gson().toJson(usersList));
		return usersList;

	}

	/**
	 * @param cell
	 * @return
	 * @throws Exception
	 */
	private static Object getCellValue(Cell cell) throws Exception {
		switch (cell.getCellTypeEnum()) {
		case STRING:
			return cell.getStringCellValue();

		case BOOLEAN:
			return cell.getBooleanCellValue();

		case NUMERIC:
			return cell.getNumericCellValue();
		default:
			break;
		}

		return null;
	}

	public static void main(String[] args) {
		File myFile = new File("/Users/majidkhan/Downloads/Payment.xlsx");
		try {
			FileInputStream fis = new FileInputStream(myFile);

			List<PaymentDetailRequestModel> userDataList = new PaymentFileRead().readFile(fis);
			System.out.println("-->> Final UserData List Size -- " + userDataList.size());
			if (userDataList.size() > 0) {
				 new PaymentDataService().insertUserData(userDataList);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
