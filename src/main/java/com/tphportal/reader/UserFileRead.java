package com.tphportal.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.Gson;
import com.tphportal.model.UsersDataModel;
import com.tphportal.service.UserDataService;


public class UserFileRead {
	public List<UsersDataModel> readFile(InputStream objectData) {
		List<UsersDataModel> usersList = new ArrayList<UsersDataModel>();
		System.out.println("Inside readFile");

		// Finds the workbook instance for XLSX file
		try (XSSFWorkbook myWorkBook = new XSSFWorkbook(objectData);) {
			UsersDataModel userDataModel = null;

			System.out.println("Inside objectData");
			
			// Return first sheet from the XLSX workbook
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);

			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = mySheet.iterator();

			// Traversing over each row of XLSX file
			int rowCounter = 0;
			Boolean isStopReading = false;
			
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();

				userDataModel = new UsersDataModel();
				if (rowCounter > 2 && !isStopReading) {
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();

						switch (cell.getColumnIndex()) {
						case 0:
							System.out.println("Case-- 0-- "+getCellValue(cell));
							if(getCellValue(cell)== null ) {
								//rowCounter = -2;
								isStopReading = true;
							}
							else {
							Object mobileNumber = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
							userDataModel.setUserName(String.valueOf(mobileNumber));
							}
							break;
						case 1:
							System.out.println("Case-- 1-- "+getCellValue(cell));
							if(!isStopReading) {
								Object password = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								userDataModel.setPassword(String.valueOf(password));
							}
							
							break;
						case 2:
							System.out.println("Case-- 2-- "+getCellValue(cell));
							userDataModel.setEmail(String.valueOf(getCellValue(cell)));
							break;
							
						case 3:
							System.out.println("Case-- 3-- "+getCellValue(cell));
							
							if(!isStopReading) {
								Object userMobile = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								userDataModel.setUserMobile(String.valueOf(userMobile));
							}
							break;
							
						case 4:
							System.out.println("Case-- 4-- "+getCellValue(cell));
							userDataModel.setFirstName(String.valueOf(getCellValue(cell)));
							break;
							
						case 5:
							System.out.println("Case-- 5-- "+getCellValue(cell));
							userDataModel.setCompanyName(String.valueOf(getCellValue(cell)));
							break;
							
						case 6:
							System.out.println("Case-- 6-- "+getCellValue(cell));
							userDataModel.setAddressOne(String.valueOf(getCellValue(cell)));
							break;
							
						case 7:
							System.out.println("Case-- 7-- "+getCellValue(cell));
							String buyer = (String)getCellValue(cell);
							userDataModel.setIsBuyer(buyer);
							break;
							
						case 8:
							System.out.println("Case-- 8-- "+getCellValue(cell));
							userDataModel.setIsSeller(String.valueOf(getCellValue(cell)));
							break;
							
						case 9:
							System.out.println("Case-- 9-- "+getCellValue(cell));
							
							if(!isStopReading) {
								if(getCellValue(cell) != null ) {
								Object pinCode = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								userDataModel.setPinCode(String.valueOf(pinCode));
								}
							}
							break;
							
						case 10:
							System.out.println("Case gst-- 10-- "+getCellValue(cell));
							
							if(!isStopReading && getCellValue(cell) != null) {
								userDataModel.setGstNo(String.valueOf(getCellValue(cell)));
							}
							break;
							
						case 11:
							System.out.println("Case--11-- "+getCellValue(cell));
							userDataModel.setPhone(String.valueOf(getCellValue(cell)));
							break;
							
						case 12:
							System.out.println("Case--12-- "+getCellValue(cell));
							if(!isStopReading) { 
								if(getCellValue(cell)!= null ) {
								Object countryId = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								userDataModel.setCountryId(String.valueOf(countryId));
								}
							}
							
							break;
							
						case 13:
							System.out.println("Case--13-- "+getCellValue(cell));
							
							if(!isStopReading) {
								if(getCellValue(cell)!= null ) {
								Object stateId = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								userDataModel.setStateId(String.valueOf(stateId));
								}
							}
							break;
							
						case 14:
							System.out.println("Case--14-- "+getCellValue(cell));
							
							if(!isStopReading) {
								if(getCellValue(cell)!= null ) {
								Object cityId = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								userDataModel.setCityId(String.valueOf(cityId));
								}
							}
							break;
						}
					}
					if(!isStopReading)
					usersList.add(userDataModel);
				}
				rowCounter++;
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("usersList -- "+new Gson().toJson(usersList));
		return usersList;

	}

	/**
	 * @param cell
	 * @return
	 * @throws Exception
	 */
	private static Object getCellValue(Cell cell) throws Exception {
		switch (cell.getCellTypeEnum()) {
		case STRING:
			return cell.getStringCellValue();

		case BOOLEAN:
			return cell.getBooleanCellValue();

		case NUMERIC:
			return cell.getNumericCellValue();
		default:
			break;
		}

		return null;
	}
	
	public static void main(String[] args) {
		File myFile = new File("/Users/majidkhan/Downloads/Seller.xlsx");
        try {
			FileInputStream fis = new FileInputStream(myFile);
			
				List<UsersDataModel> userDataList = new UserFileRead().readFile(fis);
				 System.out.println("-->> Final UserData List Size -- "+userDataList.size());
				 if(userDataList.size()>0){
					 new UserDataService().insertUserData(userDataList);
				 }
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

}
