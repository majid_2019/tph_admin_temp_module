/**
 * 
 */
package com.tphportal.model;

import java.math.BigDecimal;

/**
 * @author majidkhan
 *
 */

public class PaymentDetailRequestModel {
	private String productId;
	private String payType;
	private String accountNumber;
	private String checkNumber;
	private String receiptNumber;
	private String ifsc;
	private String transactionId;
	private Integer sellerID;
	private Integer buyerId;
	private BigDecimal dashboardAmount;
	private BigDecimal tphBaseAmount;
	private BigDecimal tphTotalAmount;
	private BigDecimal cgst;
	private BigDecimal sgst;
	private BigDecimal igst;
	private Boolean isPaymentSuccess;
	private String paymentStatus;
	private String mobileNumber;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Integer getSellerID() {
		return sellerID;
	}

	public void setSellerID(Integer sellerID) {
		this.sellerID = sellerID;
	}

	public Integer getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(Integer buyerId) {
		this.buyerId = buyerId;
	}

	public BigDecimal getDashboardAmount() {
		return dashboardAmount;
	}

	public void setDashboardAmount(BigDecimal dashboardAmount) {
		this.dashboardAmount = dashboardAmount;
	}

	public BigDecimal getTphBaseAmount() {
		return tphBaseAmount;
	}

	public void setTphBaseAmount(BigDecimal tphBaseAmount) {
		this.tphBaseAmount = tphBaseAmount;
	}

	public BigDecimal getTphTotalAmount() {
		return tphTotalAmount;
	}

	public void setTphTotalAmount(BigDecimal tphTotalAmount) {
		this.tphTotalAmount = tphTotalAmount;
	}

	public BigDecimal getCgst() {
		return cgst;
	}

	public void setCgst(BigDecimal cgst) {
		this.cgst = cgst;
	}

	public BigDecimal getSgst() {
		return sgst;
	}

	public void setSgst(BigDecimal sgst) {
		this.sgst = sgst;
	}

	public BigDecimal getIgst() {
		return igst;
	}

	public void setIgst(BigDecimal igst) {
		this.igst = igst;
	}

	public Boolean getIsPaymentSuccess() {
		return isPaymentSuccess;
	}

	public void setIsPaymentSuccess(Boolean isPaymentSuccess) {
		this.isPaymentSuccess = isPaymentSuccess;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
}
