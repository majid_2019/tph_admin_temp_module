/**
 * 
 */
package com.tphportal.model;

/**
 * @author majidkhan
 *
 */

public class SellerBaseRequestModel {
	private Integer id;
	private Integer userId;
	private Integer stateId;
	private Integer cityId;
	private Integer uomId;
	private Integer countryId;
	private Integer industrialTypeId;
	private Integer catTypeId;
	private Integer subCatTypeId;
	private Integer productId;
	private String serviceType;
	private Integer ipTypeID; // IndustrialProduct Type Id
	private Integer serviceCatId; // industrial_product_services Type Id

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getUomId() {
		return uomId;
	}

	public void setUomId(Integer uomId) {
		this.uomId = uomId;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public Integer getIndustrialTypeId() {
		return industrialTypeId;
	}

	public void setIndustrialTypeId(Integer industrialTypeId) {
		this.industrialTypeId = industrialTypeId;
	}

	public Integer getCatTypeId() {
		return catTypeId;
	}

	public void setCatTypeId(Integer catTypeId) {
		this.catTypeId = catTypeId;
	}

	public Integer getSubCatTypeId() {
		return subCatTypeId;
	}

	public void setSubCatTypeId(Integer subCatTypeId) {
		this.subCatTypeId = subCatTypeId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Integer getIpTypeID() {
		return ipTypeID;
	}

	public void setIpTypeID(Integer ipTypeID) {
		this.ipTypeID = ipTypeID;
	}

	public Integer getServiceCatId() {
		return serviceCatId;
	}

	public void setServiceCatId(Integer serviceCatId) {
		this.serviceCatId = serviceCatId;
	}
}
