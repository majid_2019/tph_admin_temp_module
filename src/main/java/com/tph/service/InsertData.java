/**
 * 
 */
package com.tph.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.tph.model.UsersDataModel;
import com.tph.util.DBConnection;

/**
 * @author majidkhan
 *
 */
public class InsertData {

	private static final String insertQuery = "insert into tph_db.users (email, user_mobile, first_name, company, Address_1, pin_code, GST_NO, country_id, city_id, state_id, ip_address, username, password,last_name) value (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String selectQuery = "select a.id, b.id, c.id from tph_db.country_master a, tph_db.state_master b, tph_db.city_master c where a.name like ? and b.state like ? and c.city like ?";

	private static final String countryQuery = "select a.id, a.name from tph_db.country_master a";
	private static final String stateQuery = "select a.id, a.state from tph_db.state_master a";
	private static final String cityQuery = "select a.id, a.city from tph_db.city_master a";

	/**
	 * @param modelList
	 */
	public void insertModel(List<UsersDataModel> modelList) {
		try (Connection connection = DBConnection.getConnection()) {
			PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);

			Map<String, String> countryMap = countryMap(connection);
			Map<String, String> stateMap = stateMap(connection);
			Map<String, String> cityMap = cityMap(connection);

			int totalRecord = 0;
			for (UsersDataModel usersDataModel : modelList) {
				String countryId = countryIdFromMap(countryMap, usersDataModel.getCountryName());
				String stateId = stateIdFromMap(stateMap, usersDataModel.getStateName());
				String cityId = cityIdFromMap(cityMap, usersDataModel.getCityName());

				if(usersDataModel.getEmail() != null) {
					preparedStatement.setString(1, usersDataModel.getEmail());
				}
				else {
					preparedStatement.setString(1, "abc@abc.com");
				}
				
				if(usersDataModel.getUserMobile() != null) {
					preparedStatement.setLong(2, Long.parseLong(usersDataModel.getUserMobile().toString()));
				}
				else {
					preparedStatement.setLong(2, 000000000);
				}
				
				if(usersDataModel.getFirstName() != null) {
					preparedStatement.setString(3, usersDataModel.getFirstName());
				}
				else {
					preparedStatement.setString(3, "Default Name");
				}
				
				if(usersDataModel.getCompanyName() != null) {
					preparedStatement.setString(4, usersDataModel.getCompanyName());	
				}
				else {
					preparedStatement.setString(4, "Default Company");
				}
				
				
				if(usersDataModel.getAddressOne() != null) {
					preparedStatement.setString(5, usersDataModel.getAddressOne());
				}
				else {
					preparedStatement.setString(5, "Tph default");
				}
				
				if(usersDataModel.getPinCode() != null) {
					preparedStatement.setInt(6, usersDataModel.getPinCode());
				}
				else {
					preparedStatement.setInt(6, 0);
				}
				
				if(usersDataModel.getGstNo() != null) {
					preparedStatement.setString(7, usersDataModel.getGstNo());
				}
				else {
					preparedStatement.setString(7, "");
				}
				
				if(countryId != null) {
					preparedStatement.setInt(8, Integer.valueOf(countryId));
				}
				else {
					preparedStatement.setInt(8, 0);
				}
				
				if(stateId != null) {
					preparedStatement.setInt(9, Integer.valueOf(stateId));	
				}
				else {
					preparedStatement.setInt(9, 0);
				}
				
				if(cityId != null) {
					preparedStatement.setInt(10, Integer.valueOf(cityId));	
				}
				else {
					preparedStatement.setInt(10, 0);
				}
				
				preparedStatement.setString(11, "172.0.0.1");
				preparedStatement.setString(12, usersDataModel.getEmail());
				preparedStatement.setString(13, "abcd");
				preparedStatement.setString(14, "");

				int i = preparedStatement.executeUpdate();
				totalRecord = totalRecord+i;
				//System.out.println("-->>> Total inserted Data --<<< " + i);
			}
			preparedStatement.close();
			System.out.println("Total record inserted -- >>> "+totalRecord);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param modelList
	 * @param connection
	 * @return
	 */
	private Map<String, String> countryMap(Connection connection) throws Exception {
		PreparedStatement ps = connection.prepareStatement(countryQuery);

		Map<String, String> countryMap = new HashMap<String, String>();

		ResultSet resultSetAppData = ps.executeQuery();

		if (resultSetAppData != null) {
			while (resultSetAppData.next()) {
				countryMap.put(resultSetAppData.getString("id"), resultSetAppData.getString("name"));
			}
		}
		ps.close();
		return countryMap;
	}

	/**
	 * @param modelList
	 * @param connection
	 * @return
	 */
	private Map<String, String> stateMap(Connection connection) throws Exception {
		PreparedStatement ps = connection.prepareStatement(stateQuery);

		Map<String, String> stateMap = new HashMap<String, String>();

		ResultSet resultSetAppData = ps.executeQuery();

		if (resultSetAppData != null) {
			while (resultSetAppData.next()) {
				stateMap.put(resultSetAppData.getString("id"), resultSetAppData.getString("state"));
			}
		}
		ps.close();
		return stateMap;
	}

	/**
	 * @param modelList
	 * @param connection
	 * @return
	 */
	private Map<String, String> cityMap(Connection connection) throws Exception {
		PreparedStatement ps = connection.prepareStatement(cityQuery);

		Map<String, String> cityMap = new HashMap<String, String>();

		ResultSet resultSetAppData = ps.executeQuery();

		if (resultSetAppData != null) {
			while (resultSetAppData.next()) {
				cityMap.put(resultSetAppData.getString("id"), resultSetAppData.getString("city"));
			}
		}
		ps.close();
		return cityMap;
	}

	/**
	 * @param countryMap
	 * @return
	 * @throws Exception
	 */
	private String countryIdFromMap(Map<String, String> countryMap, String countryName) throws Exception {
		Iterator it = countryMap.entrySet().iterator();
		String countryId = "0";
		
		if(countryName != null)
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			if (countryName.toLowerCase().equalsIgnoreCase(pair.getValue().toString().toLowerCase())) {
				countryId = (String) pair.getKey();
			}
			//it.remove();
		}
		return countryId;
	}

	/**
	 * @param countryMap
	 * @return
	 * @throws Exception
	 */
	private String stateIdFromMap(Map<String, String> countryMap, String stateName) throws Exception {
		Iterator it = countryMap.entrySet().iterator();
		String stateId = "0";
		if(stateName != null)
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			if (stateName != null)
				if (stateName.toLowerCase().equalsIgnoreCase(pair.getValue().toString().toLowerCase())) {
					stateId = (String) pair.getKey();
				}
		}
		return stateId;
	}

	/**
	 * @param countryMap
	 * @return
	 * @throws Exception
	 */
	private String cityIdFromMap(Map<String, String> countryMap, String countryName) throws Exception {
		Iterator it = countryMap.entrySet().iterator();
		String cityId = "0";

		if(countryName != null)
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			if (countryName.toLowerCase().equalsIgnoreCase(pair.getValue().toString().toLowerCase())) {
				cityId = (String) pair.getKey();
			}
			//it.remove();
		}
		return cityId;
	}

}
