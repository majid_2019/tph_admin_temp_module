/**
 * 
 */
package com.tph.handler;

import java.util.List;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.tphportal.constant.Constants;
import com.tphportal.model.PaymentDetailRequestModel;
import com.tphportal.model.UsersDataModel;
import com.tphportal.reader.PaymentFileRead;
import com.tphportal.reader.UserFileRead;
import com.tphportal.service.PaymentDataService;
import com.tphportal.service.UserDataService;



/**
 * @author majidkhan
 *
 */
public class SaveDataHandler implements RequestHandler<S3Event, String> {
	
	private final static Gson gson = new Gson();
	private final static ObjectMapper objectMapper = new ObjectMapper();
	

	@Override
	public String handleRequest(S3Event event, com.amazonaws.services.lambda.runtime.Context context) {
		
		try {
			//Here Give your aws user credentials instead of xxxx
			 BasicAWSCredentials awscreds = new BasicAWSCredentials(Constants.awsAccessKey,Constants.awsSecretKey);
			 
			//AmazonS3Client s3Client = new AmazonS3Client(new DefaultAWSCredentialsProviderChain());
			//AmazonS3 s3Client = new AmazonS3Client(DefaultAWSCredentialsProviderChain.getInstance());
			AmazonS3 s3Client = AmazonS3ClientBuilder.standard()   
    	    	    .withRegion("ap-south-1")
    	    	    .withCredentials(new AWSStaticCredentialsProvider(awscreds))
    	    	    .build();

			for (S3EventNotificationRecord record : event.getRecords()) {
				System.out.println("record -- "+new Gson().toJson(record));
				String s3Key = record.getS3().getObject().getKey();
				String s3Bucket = record.getS3().getBucket().getName();
				
				System.out.println("found id: " + s3Bucket + " " + s3Key);
				
				S3Object object = s3Client.getObject(new GetObjectRequest(s3Bucket, s3Key));

	            System.out.println("Content-Type: " + object.getObjectMetadata().getContentType());
				System.out.println("object object-- "+object);
				
				 S3ObjectInputStream objectData = object.getObjectContent();
				
				 System.out.println("Content: ");
		           // displayTextInputStream(object.getObjectContent());
				
				 if(s3Key.contains("Users")) {
					 System.out.println("--->>> Users--->>>");
					 List<UsersDataModel> userDataList = new UserFileRead().readFile(objectData);
					 System.out.println("-->> Final UserData List Size -- "+userDataList.size());
					 if(userDataList.size()>0){
						 new UserDataService().insertUserData(userDataList);
					 }
				 }
				 else if(s3Key.contains("Payment")) {
					 System.out.println("---->>>> Payment--->>>");
					 List<PaymentDetailRequestModel> userDataList = new PaymentFileRead().readFile(objectData);
					 System.out.println("-->> Final Payment List Size -- "+userDataList.size());
					 if(userDataList.size()>0){
						 new PaymentDataService().insertUserData(userDataList);
					 }
				 }
				 
				/* S3ObjectInputStream s3ObjectInputStream = object.getObjectContent();
				 System.out.println("11111");
				 XSSFWorkbook workbook = new XSSFWorkbook(s3ObjectInputStream);
				 XSSFSheet sheet = workbook.getSheetAt(0);
				 Iterator<Row> rowIterator = sheet.iterator();
				 System.out.println("2222");
                 while (rowIterator.hasNext()) 
                {
                	 System.out.println("in while");
                }*/
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
}
