package com.tph.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import com.google.gson.Gson;
import com.tph.businesslogic.UserFileRead;
import com.tph.model.UsersDataModel;
import com.tph.service.InsertData;

public class TestFileUpload {

	public static void main(String[] args) {
		File myFile = new File("/Users/majidkhan/Downloads/seller.xlsx");
        try {
			FileInputStream fis = new FileInputStream(myFile);
			
			List<UsersDataModel> modelList = new UserFileRead().readFile(fis);
			System.out.println(new Gson().toJson(modelList));
			
			if(modelList.size() > 0) {
				/**
				 * Insert Data into Database
				 */
				new InsertData().insertModel(modelList);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}
