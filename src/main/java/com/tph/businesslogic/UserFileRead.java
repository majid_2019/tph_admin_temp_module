package com.tph.businesslogic;

import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.tph.model.UsersDataModel;

public class UserFileRead {
	public List<UsersDataModel> readFile(FileInputStream fis) {
		List<UsersDataModel> usersList = new ArrayList<UsersDataModel>();

		// Finds the workbook instance for XLSX file
		try (XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);) {
			UsersDataModel userDataModel = null;

			// Return first sheet from the XLSX workbook
			XSSFSheet mySheet = myWorkBook.getSheetAt(0);

			// Get iterator to all the rows in current sheet
			Iterator<Row> rowIterator = mySheet.iterator();

			// Traversing over each row of XLSX file
			int rowCounter = 0;
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				Iterator<Cell> cellIterator = row.cellIterator();

				userDataModel = new UsersDataModel();
				if (rowCounter > 1) {
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();

						switch (cell.getColumnIndex()) {
						case 0:
							// userDataModel.setEmployeeName((String) getCellValue(cell));
							break;
						case 1:
							userDataModel.setCompanyName((String) getCellValue(cell));
							break;
						case 2:
							userDataModel.setFirstName((String) getCellValue(cell));
							break;
						case 3:
							userDataModel.setGstNo((String) getCellValue(cell));
							break;
						case 4:
							if (getCellValue(cell) != null) {
								Object mobileNumber = new BigDecimal(getCellValue(cell).toString().replaceAll("[\\s\\-()]", "")).toBigInteger();
								//BigDecimal myNumber = new BigDecimal(getCellValue(cell).toString());
								//Long myDouble = myNumber.longValue();
								userDataModel.setUserMobile(mobileNumber);
							}

							break;

						case 5:
							// userDataModel.setAl((Integer)getCellValue(cell));
							break;
						case 6:
							userDataModel.setEmail((String) getCellValue(cell));

							break;

						case 7:
							// userDataModel.setAddressOne((String) getCellValue(cell));

							break;

						case 8:
							// userDataModel.setCountryName((String) getCellValue(cell));

							break;

						case 9:
							userDataModel.setAddressOne((String) getCellValue(cell));
							break;

						case 10:
							userDataModel.setCountryName((String) getCellValue(cell));

							break;
						case 11:
							userDataModel.setStateName((String) getCellValue(cell));

							break;

						case 12:
							userDataModel.setCityName((String) getCellValue(cell));
							break;

						case 13:

							Double pinCode = (Double) getCellValue(cell);
							if (pinCode != null) {
								userDataModel.setPinCode(pinCode.intValue());
							}
							break;

						}
					}

					usersList.add(userDataModel);
				}
				rowCounter++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usersList;

	}

	/**
	 * @param cell
	 * @return
	 * @throws Exception
	 */
	private static Object getCellValue(Cell cell) throws Exception {
		switch (cell.getCellTypeEnum()) {
		case STRING:
			return cell.getStringCellValue();

		case BOOLEAN:
			return cell.getBooleanCellValue();

		case NUMERIC:
			return cell.getNumericCellValue();
		default:
			break;
		}

		return null;
	}

}
