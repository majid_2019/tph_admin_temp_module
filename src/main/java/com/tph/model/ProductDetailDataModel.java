package com.tph.model;
import java.sql.Timestamp;

public class ProductDetailDataModel {
	public Integer Id;
	public String ProductCode;
	public Integer IndustrialTypeId;
	public Integer CategoryId;
	public Integer SubCategoryId;
	public String  ProductName;
	public String  ProductSubDesc;
	public String  ProductDescription;
	public String  HsnSacCode;
	public String  ProductBrand;
	public String  MfgPurchaseYear;
	public Integer Quantity;
	public Integer UomId;
	public Timestamp ExpiryDate;
	public double PricePerUnit;
	public Boolean PriceNegotiable;
	public Integer ProductStatusId;
	public String  ProdLoc;
	public Integer CountryId;
	public Integer StateId;
	public Integer CityId;
	public double  LatestOfferPrice;
	public String  IsActive;
	public Timestamp UpdatedDate;
	public Timestamp CreatedDate;
	public Integer   UserId;
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getProductCode() {
		return ProductCode;
	}
	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}
	public Integer getIndustrialTypeId() {
		return IndustrialTypeId;
	}
	public void setIndustrialTypeId(Integer industrialTypeId) {
		IndustrialTypeId = industrialTypeId;
	}
	public Integer getCategoryId() {
		return CategoryId;
	}
	public void setCategoryId(Integer categoryId) {
		CategoryId = categoryId;
	}
	public Integer getSubCategoryId() {
		return SubCategoryId;
	}
	public void setSubCategoryId(Integer subCategoryId) {
		SubCategoryId = subCategoryId;
	}
	public String getProductName() {
		return ProductName;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public String getProductSubDesc() {
		return ProductSubDesc;
	}
	public void setProductSubDesc(String productSubDesc) {
		ProductSubDesc = productSubDesc;
	}
	public String getProductDescription() {
		return ProductDescription;
	}
	public void setProductDescription(String productDescription) {
		ProductDescription = productDescription;
	}
	public String getHsnSacCode() {
		return HsnSacCode;
	}
	public void setHsnSacCode(String hsnSacCode) {
		HsnSacCode = hsnSacCode;
	}
	public String getProductBrand() {
		return ProductBrand;
	}
	public void setProductBrand(String productBrand) {
		ProductBrand = productBrand;
	}
	public String getMfgPurchaseYear() {
		return MfgPurchaseYear;
	}
	public void setMfgPurchaseYear(String mfgPurchaseYear) {
		MfgPurchaseYear = mfgPurchaseYear;
	}
	public Integer getQuantity() {
		return Quantity;
	}
	public void setQuantity(Integer quantity) {
		Quantity = quantity;
	}
	public Integer getUomId() {
		return UomId;
	}
	public void setUomId(Integer uomId) {
		UomId = uomId;
	}
	public Timestamp getExpiryDate() {
		return ExpiryDate;
	}
	public void setExpiryDate(Timestamp expiryDate) {
		ExpiryDate = expiryDate;
	}
	public double getPricePerUnit() {
		return PricePerUnit;
	}
	public void setPricePerUnit(double pricePerUnit) {
		PricePerUnit = pricePerUnit;
	}
	public Boolean getPriceNegotiable() {
		return PriceNegotiable;
	}
	public void setPriceNegotiable(Boolean priceNegotiable) {
		PriceNegotiable = priceNegotiable;
	}
	public Integer getProductStatusId() {
		return ProductStatusId;
	}
	public void setProductStatusId(Integer productStatusId) {
		ProductStatusId = productStatusId;
	}
	public String getProdLoc() {
		return ProdLoc;
	}
	public void setProdLoc(String prodLoc) {
		ProdLoc = prodLoc;
	}
	public Integer getCountryId() {
		return CountryId;
	}
	public void setCountryId(Integer countryId) {
		CountryId = countryId;
	}
	public Integer getStateId() {
		return StateId;
	}
	public void setStateId(Integer stateId) {
		StateId = stateId;
	}
	public Integer getCityId() {
		return CityId;
	}
	public void setCityId(Integer cityId) {
		CityId = cityId;
	}
	public double getLatestOfferPrice() {
		return LatestOfferPrice;
	}
	public void setLatestOfferPrice(double latestOfferPrice) {
		LatestOfferPrice = latestOfferPrice;
	}
	public String getIsActive() {
		return IsActive;
	}
	public void setIsActive(String isActive) {
		IsActive = isActive;
	}
	public Timestamp getUpdatedDate() {
		return UpdatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		UpdatedDate = updatedDate;
	}
	public Timestamp getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		CreatedDate = createdDate;
	}
	public Integer getUserId() {
		return UserId;
	}
	public void setUserId(Integer userId) {
		UserId = userId;
	}

}
