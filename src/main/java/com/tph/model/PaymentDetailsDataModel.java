package com.tph.model;

public class PaymentDetailsDataModel {
	public Integer Id;
	public Integer ProdListAmtId;
	public Integer PayTypeMasterId;
	public Integer AccountNumber;
	public Integer CheckNumber;
	public String ReceiptNumber;
	public String BankName;
	public String RoutingCode;
	public long TransId;
	public String SellerUserId;
	public String BuyerUserId;
	public double Amount;
	public double CommissionAmount;
	public Integer getPaymentId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Integer getProdListAmtId() {
		return ProdListAmtId;
	}
	public void setProdListAmtId(Integer prodListAmtId) {
		ProdListAmtId = prodListAmtId;
	}
	public Integer getPayTypeMasterId() {
		return PayTypeMasterId;
	}
	public void setPayTypeMasterId(Integer payTypeMasterId) {
		PayTypeMasterId = payTypeMasterId;
	}
	public Integer getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		AccountNumber = accountNumber;
	}
	public Integer getCheckNumber() {
		return CheckNumber;
	}
	public void setCheckNumber(Integer checkNumber) {
		CheckNumber = checkNumber;
	}
	public String getReceiptNumber() {
		return ReceiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		ReceiptNumber = receiptNumber;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getRoutingCode() {
		return RoutingCode;
	}
	public void setRoutingCode(String routingCode) {
		RoutingCode = routingCode;
	}
	public long getTransId() {
		return TransId;
	}
	public void setTransId(long transId) {
		TransId = transId;
	}
	public String getSellerUserId() {
		return SellerUserId;
	}
	public void setSellerUserId(String sellerUserId) {
		SellerUserId = sellerUserId;
	}
	public String getBuyerUserId() {
		return BuyerUserId;
	}
	public void setBuyerUserId(String buyerUserId) {
		BuyerUserId = buyerUserId;
	}
	public double getAmount() {
		return Amount;
	}
	public void setAmount(double amount) {
		Amount = amount;
	}
	public double getCommissionAmount() {
		return CommissionAmount;
	}
	public void setCommissionAmount(double commissionAmount) {
		CommissionAmount = commissionAmount;
	}

}
