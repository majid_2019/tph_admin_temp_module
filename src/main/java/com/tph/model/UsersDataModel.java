package com.tph.model;
import java.sql.Timestamp;

public class UsersDataModel {
	public Integer Id;
	public String IpAddress;
	public String UserName;
	public String Password;
	public String Email;
	public Object UserMobile;
	public String ActivationCode;
	public String ForgottenPasswordCode;
	public Timestamp ForgottenPasswordTime;
	public String RememberCode;
	public Timestamp CreatedOn;
	public Timestamp LastLogin;
	public String Active;
	public String FirstName;
	public String LastName;
	public String CompanyName;
	public String AddressOne;
	public String AddressTwo;
	public String AddressThree;
	public String IsBuyer;
	public String IsSeller;
	public Integer PinCode;
	public String GstNo;
	public String Phone;
	public Timestamp PasswordExpiryOn;
	public Integer LoginAttempt;
	public Integer CountryId;
	public Integer CityId;
	public Integer StateId;
	public Integer PaymentId;
	public String countryName;
	public String stateName;
	public String cityName;
	
	public Integer ProdListAmtId;
	public Integer PayTypeMasterId;
	public Integer AccountNumber;
	public Integer CheckNumber;
	public String ReceiptNumber;
	public String BankName;
	public String RoutingCode;
	public long TransId;
	public String SellerUserId;
	public String BuyerUserId;
	public double Amount;
	public double CommissionAmount;
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getIpAddress() {
		return IpAddress;
	}
	public void setIpAddress(String ipAddress) {
		IpAddress = ipAddress;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public Object getUserMobile() {
		return UserMobile;
	}
	public void setUserMobile(Object userMobile) {
		UserMobile = userMobile;
	}
	public String getActivationCode() {
		return ActivationCode;
	}
	public void setActivationCode(String activationCode) {
		ActivationCode = activationCode;
	}
	public String getForgottenPasswordCode() {
		return ForgottenPasswordCode;
	}
	public void setForgottenPasswordCode(String forgottenPasswordCode) {
		ForgottenPasswordCode = forgottenPasswordCode;
	}
	public Timestamp getForgottenPasswordTime() {
		return ForgottenPasswordTime;
	}
	public void setForgottenPasswordTime(Timestamp forgottenPasswordTime) {
		ForgottenPasswordTime = forgottenPasswordTime;
	}
	public String getRememberCode() {
		return RememberCode;
	}
	public void setRememberCode(String rememberCode) {
		RememberCode = rememberCode;
	}
	public Timestamp getCreatedOn() {
		return CreatedOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		CreatedOn = createdOn;
	}
	public Timestamp getLastLogin() {
		return LastLogin;
	}
	public void setLastLogin(Timestamp lastLogin) {
		LastLogin = lastLogin;
	}
	public String getActive() {
		return Active;
	}
	public void setActive(String active) {
		Active = active;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}
	public String getAddressOne() {
		return AddressOne;
	}
	public void setAddressOne(String addressOne) {
		AddressOne = addressOne;
	}
	public String getAddressTwo() {
		return AddressTwo;
	}
	public void setAddressTwo(String addressTwo) {
		AddressTwo = addressTwo;
	}
	public String getAddressThree() {
		return AddressThree;
	}
	public void setAddressThree(String addressThree) {
		AddressThree = addressThree;
	}
	public String getIsBuyer() {
		return IsBuyer;
	}
	public void setIsBuyer(String isBuyer) {
		IsBuyer = isBuyer;
	}
	public String getIsSeller() {
		return IsSeller;
	}
	public void setIsSeller(String isSeller) {
		IsSeller = isSeller;
	}
	public Integer getPinCode() {
		return PinCode;
	}
	public void setPinCode(Integer pinCode) {
		PinCode = pinCode;
	}
	public String getGstNo() {
		return GstNo;
	}
	public void setGstNo(String gstNo) {
		GstNo = gstNo;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	public Timestamp getPasswordExpiryOn() {
		return PasswordExpiryOn;
	}
	public void setPasswordExpiryOn(Timestamp passwordExpiryOn) {
		PasswordExpiryOn = passwordExpiryOn;
	}
	public Integer getLoginAttempt() {
		return LoginAttempt;
	}
	public void setLoginAttempt(Integer loginAttempt) {
		LoginAttempt = loginAttempt;
	}
	public Integer getCountryId() {
		return CountryId;
	}
	public void setCountryId(Integer countryId) {
		CountryId = countryId;
	}
	public Integer getCityId() {
		return CityId;
	}
	public void setCityId(Integer cityId) {
		CityId = cityId;
	}
	public Integer getStateId() {
		return StateId;
	}
	public void setStateId(Integer stateId) {
		StateId = stateId;
	}
	public Integer getPaymentId() {
		return PaymentId;
	}
	public void setPaymentId(Integer paymentId) {
		PaymentId = paymentId;
	}
	public Integer getProdListAmtId() {
		return ProdListAmtId;
	}
	public void setProdListAmtId(Integer prodListAmtId) {
		ProdListAmtId = prodListAmtId;
	}
	public Integer getPayTypeMasterId() {
		return PayTypeMasterId;
	}
	public void setPayTypeMasterId(Integer payTypeMasterId) {
		PayTypeMasterId = payTypeMasterId;
	}
	public Integer getAccountNumber() {
		return AccountNumber;
	}
	public void setAccountNumber(Integer accountNumber) {
		AccountNumber = accountNumber;
	}
	public Integer getCheckNumber() {
		return CheckNumber;
	}
	public void setCheckNumber(Integer checkNumber) {
		CheckNumber = checkNumber;
	}
	public String getReceiptNumber() {
		return ReceiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		ReceiptNumber = receiptNumber;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getRoutingCode() {
		return RoutingCode;
	}
	public void setRoutingCode(String routingCode) {
		RoutingCode = routingCode;
	}
	public long getTransId() {
		return TransId;
	}
	public void setTransId(long transId) {
		TransId = transId;
	}
	public String getSellerUserId() {
		return SellerUserId;
	}
	public void setSellerUserId(String sellerUserId) {
		SellerUserId = sellerUserId;
	}
	public String getBuyerUserId() {
		return BuyerUserId;
	}
	public void setBuyerUserId(String buyerUserId) {
		BuyerUserId = buyerUserId;
	}
	public double getAmount() {
		return Amount;
	}
	public void setAmount(double amount) {
		Amount = amount;
	}
	public double getCommissionAmount() {
		return CommissionAmount;
	}
	public void setCommissionAmount(double commissionAmount) {
		CommissionAmount = commissionAmount;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
}
