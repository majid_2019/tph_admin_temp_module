package com.tph.util;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {

	/**
	 * QA
	 */
/*	private static String userName = "majid";
	private static String password = "Majidkhan#$54";
	private static String url = "jdbc:mysql://tph.cwzzm606rl2y.ap-south-1.rds.amazonaws.com:7095/tph_db"; // QA
	private static String driver = "com.mysql.cj.jdbc.Driver";*/

	/**
	 * Prod
	 */
	private static String userName = "majid";
	private static String password = "Majidkhan#$54";
	private static String url = "jdbc:mysql://prod-tph.cwzzm606rl2y.ap-south-1.rds.amazonaws.com:7095/tph_db"; // Prod
	private static String driver = "com.mysql.cj.jdbc.Driver";
	

	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		} catch (Exception e) {
			System.out.println("DBConnectionUtility Exception Message:::" + e.getMessage());
		}
		
		return conn;
	}

}
