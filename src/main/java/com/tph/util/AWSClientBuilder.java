package com.tph.util;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.Future;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaAsync;
import com.amazonaws.services.lambda.AWSLambdaAsyncClientBuilder;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.StorageClass;
import com.tphportal.constant.Constants;

/**
 * Provides methods related to AWS and AWS Lambda
 *
 */

public final class AWSClientBuilder {

	private AWSClientBuilder() {
	}

	private static final String ACCESS_KEY = Constants.awsAccessKey;
	private static final String SECRET_KEY = Constants.awsSecretKey;
	private static BasicAWSCredentials creds = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
	private static AWSLambda client = null;
	private static AWSLambdaAsync asynClient = null;

	/**
	 * Provides the AWS client specific to ES AWS account
	 * 
	 * @return AWSLambda - Object of the AWS Lambda to access the function of it.
	 */
	public static AWSLambda getAWSClient() {
		if (client != null) {
			return client;
		}
		return client = AWSLambdaClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
				.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
	}

	/**
	 * Provides the AWS client specific to ES AWS account to access AWS Lambda
	 * asynchronously
	 * 
	 * @return AWSLambdaAsync - Object of the AWS Lambda to access the function of
	 *         it.
	 */
	public static AWSLambdaAsync getAWSAysncClient() {
		if (asynClient != null) {
			return asynClient;
		}
		return asynClient = AWSLambdaAsyncClientBuilder.standard().withRegion(Regions.AP_SOUTH_1)
				.withCredentials(new AWSStaticCredentialsProvider(creds)).build();
	}

	/**
	 * Invokes the lambda function provided in the parameters.
	 * 
	 * @param functionName   - Lambda Function to be invoked
	 * @param requestPayload - Json request to send to lambda function
	 * @return String - Json String Response from the lambda function
	 */
	public static String invokeLambdaFunction(String functionName, String requestPayload) {
		System.out.println(" - invokeLambdaFunction() : functionName - " + functionName + " requestPayload - " + requestPayload);
		String response = null;

		try {
			InvokeRequest invokeRequest = new InvokeRequest();
			invokeRequest.setFunctionName(functionName);
			invokeRequest.setPayload(requestPayload);

			AWSLambda lClient = getAWSClient();
			ByteBuffer responsePayload = lClient.invoke(invokeRequest).getPayload();
			response = StandardCharsets.UTF_8.decode(responsePayload).toString();

			System.out.println("invokeLambdaFunction() : response - " + response);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return response;

	}

	/**
	 * Invokes the lambda function provided in the parameters.
	 * 
	 * @param functionName   - Lambda Function to be invoked
	 * @param requestPayload - Json request to send to lambda function
	 * @return String - Json String Response from the lambda function
	 */
	public static void invokeAsyncLambdaFunction(String functionName, String requestPayload) {
		
		System.out.println("invokeAsyncLambdaFunction() : functionName - " + functionName
				+ " requestPayload - " + requestPayload);

		AWSLambdaAsync lClient = getAWSAysncClient();
		InvokeRequest invokeRequest = new InvokeRequest();
		invokeRequest.setFunctionName(functionName);
		invokeRequest.setPayload(requestPayload);
		invokeRequest.setInvocationType("Event");
		
		 Future<InvokeResult> future_res = lClient.invokeAsync(invokeRequest);
		 try {
			 InvokeResult res = future_res.get();
			 
			 System.out.println("AWSClientBuilder - invokeAsyncLambdaFunction() : Cir Request Served."
				 		+ " StatusCode - " + res.getStatusCode());
		 } catch(Exception e) {
			 System.out.println("AWSClientBuilder - invokeAsyncLambdaFunction() : Exception. - " 
					 + e.getMessage());
			 System.out.println("AWSClientBuilder - invokeAsyncLambdaFunction() : Exception. - "
					 + e.getStackTrace());
			 e.printStackTrace();
		 }
	}

	public static String saveFile(InputStream inputStream, String mime, String bucketName, String folderName,
			String fileName, long size) {
		String finalFileName = folderName + "/" + fileName;
		try {

			AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
			AmazonS3 s3client = new AmazonS3Client(credentials);

			System.out.println("Bucket Regios: " + s3client.getRegion() + " " + Regions.getCurrentRegion());

			ObjectMetadata omd = new ObjectMetadata();
			omd.setContentLength(size);
			omd.setContentType(mime);

			PutObjectRequest putObject = new PutObjectRequest(bucketName, finalFileName, inputStream, omd)
					.withStorageClass(StorageClass.StandardInfrequentAccess)
					.withCannedAcl(CannedAccessControlList.PublicRead);

			s3client.putObject(putObject);

		} catch (Exception e) {
			System.out.println("Error Occured while Uploading : " + e);
			e.printStackTrace();
			return null;
		}
		return finalFileName;

	}

}
