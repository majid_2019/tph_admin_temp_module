/*
 * This file is generated by jOOQ.
*/
package com.tph.jooq.tph_db.tables.records;


import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record22;
import org.jooq.Row22;
import org.jooq.impl.UpdatableRecordImpl;

import com.tph.jooq.tph_db.tables.ExcessWorkMaster;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ExcessWorkMasterRecord extends UpdatableRecordImpl<ExcessWorkMasterRecord> implements Record22<Integer, Integer, String, String, String, String, String, String, String, Integer, Integer, String, Integer, Integer, String, Integer, String, String, Timestamp, String, Timestamp, Timestamp> {

    private static final long serialVersionUID = 1609849440;

    /**
     * Setter for <code>tph_db.excess_work_master.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.category_id</code>.
     */
    public void setCategoryId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.category_id</code>.
     */
    public Integer getCategoryId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.excess_work_name</code>.
     */
    public void setExcessWorkName(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.excess_work_name</code>.
     */
    public String getExcessWorkName() {
        return (String) get(2);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.excess_work_description</code>.
     */
    public void setExcessWorkDescription(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.excess_work_description</code>.
     */
    public String getExcessWorkDescription() {
        return (String) get(3);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.company_name</code>.
     */
    public void setCompanyName(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.company_name</code>.
     */
    public String getCompanyName() {
        return (String) get(4);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.contact_persson_name</code>.
     */
    public void setContactPerssonName(String value) {
        set(5, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.contact_persson_name</code>.
     */
    public String getContactPerssonName() {
        return (String) get(5);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.mobile_no</code>.
     */
    public void setMobileNo(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.mobile_no</code>.
     */
    public String getMobileNo() {
        return (String) get(6);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.email_id</code>.
     */
    public void setEmailId(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.email_id</code>.
     */
    public String getEmailId() {
        return (String) get(7);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.pin_code</code>.
     */
    public void setPinCode(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.pin_code</code>.
     */
    public String getPinCode() {
        return (String) get(8);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.country_id</code>.
     */
    public void setCountryId(Integer value) {
        set(9, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.country_id</code>.
     */
    public Integer getCountryId() {
        return (Integer) get(9);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.state_id</code>.
     */
    public void setStateId(Integer value) {
        set(10, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.state_id</code>.
     */
    public Integer getStateId() {
        return (Integer) get(10);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.gst_no</code>.
     */
    public void setGstNo(String value) {
        set(11, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.gst_no</code>.
     */
    public String getGstNo() {
        return (String) get(11);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.intustry_type_id</code>.
     */
    public void setIntustryTypeId(Integer value) {
        set(12, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.intustry_type_id</code>.
     */
    public Integer getIntustryTypeId() {
        return (Integer) get(12);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.city_id</code>.
     */
    public void setCityId(Integer value) {
        set(13, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.city_id</code>.
     */
    public Integer getCityId() {
        return (Integer) get(13);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.address</code>.
     */
    public void setAddress(String value) {
        set(14, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.address</code>.
     */
    public String getAddress() {
        return (String) get(14);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.excess_work_type_</code>.
     */
    public void setExcessWorkType_(Integer value) {
        set(15, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.excess_work_type_</code>.
     */
    public Integer getExcessWorkType_() {
        return (Integer) get(15);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.excess_work_type_description</code>.
     */
    public void setExcessWorkTypeDescription(String value) {
        set(16, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.excess_work_type_description</code>.
     */
    public String getExcessWorkTypeDescription() {
        return (String) get(16);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.machine_available_required</code>.
     */
    public void setMachineAvailableRequired(String value) {
        set(17, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.machine_available_required</code>.
     */
    public String getMachineAvailableRequired() {
        return (String) get(17);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.live_till</code>.
     */
    public void setLiveTill(Timestamp value) {
        set(18, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.live_till</code>.
     */
    public Timestamp getLiveTill() {
        return (Timestamp) get(18);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.is_active</code>.
     */
    public void setIsActive(String value) {
        set(19, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.is_active</code>.
     */
    public String getIsActive() {
        return (String) get(19);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.updated_date</code>.
     */
    public void setUpdatedDate(Timestamp value) {
        set(20, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.updated_date</code>.
     */
    public Timestamp getUpdatedDate() {
        return (Timestamp) get(20);
    }

    /**
     * Setter for <code>tph_db.excess_work_master.created_date</code>.
     */
    public void setCreatedDate(Timestamp value) {
        set(21, value);
    }

    /**
     * Getter for <code>tph_db.excess_work_master.created_date</code>.
     */
    public Timestamp getCreatedDate() {
        return (Timestamp) get(21);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record22 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row22<Integer, Integer, String, String, String, String, String, String, String, Integer, Integer, String, Integer, Integer, String, Integer, String, String, Timestamp, String, Timestamp, Timestamp> fieldsRow() {
        return (Row22) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row22<Integer, Integer, String, String, String, String, String, String, String, Integer, Integer, String, Integer, Integer, String, Integer, String, String, Timestamp, String, Timestamp, Timestamp> valuesRow() {
        return (Row22) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.CATEGORY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.EXCESS_WORK_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.EXCESS_WORK_DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.COMPANY_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field6() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.CONTACT_PERSSON_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field7() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.MOBILE_NO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.EMAIL_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field9() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.PIN_CODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field10() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.COUNTRY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field11() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.STATE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field12() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.GST_NO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field13() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.INTUSTRY_TYPE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field14() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.CITY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field15() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.ADDRESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field16() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.EXCESS_WORK_TYPE_;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field17() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.EXCESS_WORK_TYPE_DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field18() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.MACHINE_AVAILABLE_REQUIRED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field19() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.LIVE_TILL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field20() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.IS_ACTIVE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field21() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.UPDATED_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field22() {
        return ExcessWorkMaster.EXCESS_WORK_MASTER.CREATED_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getCategoryId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getExcessWorkName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getExcessWorkDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getCompanyName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value6() {
        return getContactPerssonName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value7() {
        return getMobileNo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getEmailId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value9() {
        return getPinCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value10() {
        return getCountryId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value11() {
        return getStateId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value12() {
        return getGstNo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value13() {
        return getIntustryTypeId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value14() {
        return getCityId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value15() {
        return getAddress();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value16() {
        return getExcessWorkType_();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value17() {
        return getExcessWorkTypeDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value18() {
        return getMachineAvailableRequired();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value19() {
        return getLiveTill();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value20() {
        return getIsActive();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value21() {
        return getUpdatedDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value22() {
        return getCreatedDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value2(Integer value) {
        setCategoryId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value3(String value) {
        setExcessWorkName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value4(String value) {
        setExcessWorkDescription(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value5(String value) {
        setCompanyName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value6(String value) {
        setContactPerssonName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value7(String value) {
        setMobileNo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value8(String value) {
        setEmailId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value9(String value) {
        setPinCode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value10(Integer value) {
        setCountryId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value11(Integer value) {
        setStateId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value12(String value) {
        setGstNo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value13(Integer value) {
        setIntustryTypeId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value14(Integer value) {
        setCityId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value15(String value) {
        setAddress(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value16(Integer value) {
        setExcessWorkType_(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value17(String value) {
        setExcessWorkTypeDescription(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value18(String value) {
        setMachineAvailableRequired(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value19(Timestamp value) {
        setLiveTill(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value20(String value) {
        setIsActive(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value21(Timestamp value) {
        setUpdatedDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord value22(Timestamp value) {
        setCreatedDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExcessWorkMasterRecord values(Integer value1, Integer value2, String value3, String value4, String value5, String value6, String value7, String value8, String value9, Integer value10, Integer value11, String value12, Integer value13, Integer value14, String value15, Integer value16, String value17, String value18, Timestamp value19, String value20, Timestamp value21, Timestamp value22) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        value10(value10);
        value11(value11);
        value12(value12);
        value13(value13);
        value14(value14);
        value15(value15);
        value16(value16);
        value17(value17);
        value18(value18);
        value19(value19);
        value20(value20);
        value21(value21);
        value22(value22);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ExcessWorkMasterRecord
     */
    public ExcessWorkMasterRecord() {
        super(ExcessWorkMaster.EXCESS_WORK_MASTER);
    }

    /**
     * Create a detached, initialised ExcessWorkMasterRecord
     */
    public ExcessWorkMasterRecord(Integer id, Integer categoryId, String excessWorkName, String excessWorkDescription, String companyName, String contactPerssonName, String mobileNo, String emailId, String pinCode, Integer countryId, Integer stateId, String gstNo, Integer intustryTypeId, Integer cityId, String address, Integer excessWorkType_, String excessWorkTypeDescription, String machineAvailableRequired, Timestamp liveTill, String isActive, Timestamp updatedDate, Timestamp createdDate) {
        super(ExcessWorkMaster.EXCESS_WORK_MASTER);

        set(0, id);
        set(1, categoryId);
        set(2, excessWorkName);
        set(3, excessWorkDescription);
        set(4, companyName);
        set(5, contactPerssonName);
        set(6, mobileNo);
        set(7, emailId);
        set(8, pinCode);
        set(9, countryId);
        set(10, stateId);
        set(11, gstNo);
        set(12, intustryTypeId);
        set(13, cityId);
        set(14, address);
        set(15, excessWorkType_);
        set(16, excessWorkTypeDescription);
        set(17, machineAvailableRequired);
        set(18, liveTill);
        set(19, isActive);
        set(20, updatedDate);
        set(21, createdDate);
    }

	@Override
	public Integer component1() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component2() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component3() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component4() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component5() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component6() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component7() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component8() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component9() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component10() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component11() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component12() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component13() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component14() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component15() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component16() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component17() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component18() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component19() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component20() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component21() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component22() {
		// TODO Auto-generated method stub
		return null;
	}
}
