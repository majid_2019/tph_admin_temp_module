/*
 * This file is generated by jOOQ.
*/
package com.tph.jooq.tph_db.tables.records;


import java.sql.Date;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record8;
import org.jooq.Row8;
import org.jooq.impl.UpdatableRecordImpl;

import com.tph.jooq.tph_db.tables.BankHolidayMaster;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class BankHolidayMasterRecord extends UpdatableRecordImpl<BankHolidayMasterRecord> implements Record8<Long, String, Date, Date, Date, Integer, Byte, String> {

    private static final long serialVersionUID = 41345698;

    /**
     * Setter for <code>tph_db.bank_holiday_master.id</code>.
     */
    public void setId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.id</code>.
     */
    public Long getId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>tph_db.bank_holiday_master.name</code>.
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.name</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>tph_db.bank_holiday_master.from_date</code>.
     */
    public void setFromDate(Date value) {
        set(2, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.from_date</code>.
     */
    public Date getFromDate() {
        return (Date) get(2);
    }

    /**
     * Setter for <code>tph_db.bank_holiday_master.to_date</code>.
     */
    public void setToDate(Date value) {
        set(3, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.to_date</code>.
     */
    public Date getToDate() {
        return (Date) get(3);
    }

    /**
     * Setter for <code>tph_db.bank_holiday_master.repayments_rescheduled_to</code>.
     */
    public void setRepaymentsRescheduledTo(Date value) {
        set(4, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.repayments_rescheduled_to</code>.
     */
    public Date getRepaymentsRescheduledTo() {
        return (Date) get(4);
    }

    /**
     * Setter for <code>tph_db.bank_holiday_master.status_enum</code>.
     */
    public void setStatusEnum(Integer value) {
        set(5, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.status_enum</code>.
     */
    public Integer getStatusEnum() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>tph_db.bank_holiday_master.processed</code>.
     */
    public void setProcessed(Byte value) {
        set(6, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.processed</code>.
     */
    public Byte getProcessed() {
        return (Byte) get(6);
    }

    /**
     * Setter for <code>tph_db.bank_holiday_master.description</code>.
     */
    public void setDescription(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>tph_db.bank_holiday_master.description</code>.
     */
    public String getDescription() {
        return (String) get(7);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record8 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row8<Long, String, Date, Date, Date, Integer, Byte, String> fieldsRow() {
        return (Row8) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row8<Long, String, Date, Date, Date, Integer, Byte, String> valuesRow() {
        return (Row8) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field3() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.FROM_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field4() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.TO_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field5() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.REPAYMENTS_RESCHEDULED_TO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field6() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.STATUS_ENUM;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Byte> field7() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.PROCESSED;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return BankHolidayMaster.BANK_HOLIDAY_MASTER.DESCRIPTION;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value3() {
        return getFromDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value4() {
        return getToDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value5() {
        return getRepaymentsRescheduledTo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value6() {
        return getStatusEnum();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Byte value7() {
        return getProcessed();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getDescription();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value1(Long value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value3(Date value) {
        setFromDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value4(Date value) {
        setToDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value5(Date value) {
        setRepaymentsRescheduledTo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value6(Integer value) {
        setStatusEnum(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value7(Byte value) {
        setProcessed(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord value8(String value) {
        setDescription(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BankHolidayMasterRecord values(Long value1, String value2, Date value3, Date value4, Date value5, Integer value6, Byte value7, String value8) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached BankHolidayMasterRecord
     */
    public BankHolidayMasterRecord() {
        super(BankHolidayMaster.BANK_HOLIDAY_MASTER);
    }

    /**
     * Create a detached, initialised BankHolidayMasterRecord
     */
    public BankHolidayMasterRecord(Long id, String name, Date fromDate, Date toDate, Date repaymentsRescheduledTo, Integer statusEnum, Byte processed, String description) {
        super(BankHolidayMaster.BANK_HOLIDAY_MASTER);

        set(0, id);
        set(1, name);
        set(2, fromDate);
        set(3, toDate);
        set(4, repaymentsRescheduledTo);
        set(5, statusEnum);
        set(6, processed);
        set(7, description);
    }

	@Override
	public Long component1() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component2() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date component3() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date component4() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Date component5() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer component6() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Byte component7() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component8() {
		// TODO Auto-generated method stub
		return null;
	}
}
