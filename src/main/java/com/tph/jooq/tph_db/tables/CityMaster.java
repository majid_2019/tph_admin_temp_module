/*
 * This file is generated by jOOQ.
*/
package com.tph.jooq.tph_db.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;

import com.tph.jooq.tph_db.Keys;
import com.tph.jooq.tph_db.TphDb;
import com.tph.jooq.tph_db.tables.records.CityMasterRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CityMaster extends TableImpl<CityMasterRecord> {

    private static final long serialVersionUID = 1273528759;

    /**
     * The reference instance of <code>tph_db.city_master</code>
     */
    public static final CityMaster CITY_MASTER = new CityMaster();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<CityMasterRecord> getRecordType() {
        return CityMasterRecord.class;
    }

    /**
     * The column <code>tph_db.city_master.id</code>.
     */
    public final TableField<CityMasterRecord, Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>tph_db.city_master.state_id</code>.
     */
    public final TableField<CityMasterRecord, Integer> STATE_ID = createField("state_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>tph_db.city_master.city</code>.
     */
    public final TableField<CityMasterRecord, String> CITY = createField("city", org.jooq.impl.SQLDataType.VARCHAR.length(100).nullable(false), this, "");

    /**
     * The column <code>tph_db.city_master.is_active</code>.
     */
    public final TableField<CityMasterRecord, String> IS_ACTIVE = createField("is_active", org.jooq.impl.SQLDataType.VARCHAR.length(2).nullable(false).defaultValue(org.jooq.impl.DSL.inline("Y", org.jooq.impl.SQLDataType.VARCHAR)), this, "");

    /**
     * Create a <code>tph_db.city_master</code> table reference
     */
    public CityMaster() {
        this("city_master", null);
    }

    /**
     * Create an aliased <code>tph_db.city_master</code> table reference
     */
    public CityMaster(String alias) {
        this(alias, CITY_MASTER);
    }

    private CityMaster(String alias, Table<CityMasterRecord> aliased) {
        this(alias, aliased, null);
    }

    private CityMaster(String alias, Table<CityMasterRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return TphDb.TPH_DB;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<CityMasterRecord, Integer> getIdentity() {
        return Keys.IDENTITY_CITY_MASTER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<CityMasterRecord> getPrimaryKey() {
        return Keys.KEY_CITY_MASTER_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<CityMasterRecord>> getKeys() {
        return Arrays.<UniqueKey<CityMasterRecord>>asList(Keys.KEY_CITY_MASTER_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CityMaster as(String alias) {
        return new CityMaster(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public CityMaster rename(String name) {
        return new CityMaster(name, null);
    }
}
