/*
 * This file is generated by jOOQ.
*/
package com.tph.jooq.tph_db.tables.records;


import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;

import com.tph.jooq.tph_db.tables.CategoriesMaster;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CategoriesMasterRecord extends UpdatableRecordImpl<CategoriesMasterRecord> implements Record5<Integer, String, String, Timestamp, Timestamp> {

    private static final long serialVersionUID = 129546018;

    /**
     * Setter for <code>tph_db.categories_master.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>tph_db.categories_master.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>tph_db.categories_master.cat_name</code>.
     */
    public void setCatName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>tph_db.categories_master.cat_name</code>.
     */
    public String getCatName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>tph_db.categories_master.is_active</code>.
     */
    public void setIsActive(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>tph_db.categories_master.is_active</code>.
     */
    public String getIsActive() {
        return (String) get(2);
    }

    /**
     * Setter for <code>tph_db.categories_master.updated_date</code>.
     */
    public void setUpdatedDate(Timestamp value) {
        set(3, value);
    }

    /**
     * Getter for <code>tph_db.categories_master.updated_date</code>.
     */
    public Timestamp getUpdatedDate() {
        return (Timestamp) get(3);
    }

    /**
     * Setter for <code>tph_db.categories_master.created_date</code>.
     */
    public void setCreatedDate(Timestamp value) {
        set(4, value);
    }

    /**
     * Getter for <code>tph_db.categories_master.created_date</code>.
     */
    public Timestamp getCreatedDate() {
        return (Timestamp) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, Timestamp, Timestamp> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, String, String, Timestamp, Timestamp> valuesRow() {
        return (Row5) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return CategoriesMaster.CATEGORIES_MASTER.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return CategoriesMaster.CATEGORIES_MASTER.CAT_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return CategoriesMaster.CATEGORIES_MASTER.IS_ACTIVE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field4() {
        return CategoriesMaster.CATEGORIES_MASTER.UPDATED_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field5() {
        return CategoriesMaster.CATEGORIES_MASTER.CREATED_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getCatName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getIsActive();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value4() {
        return getUpdatedDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value5() {
        return getCreatedDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CategoriesMasterRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CategoriesMasterRecord value2(String value) {
        setCatName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CategoriesMasterRecord value3(String value) {
        setIsActive(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CategoriesMasterRecord value4(Timestamp value) {
        setUpdatedDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CategoriesMasterRecord value5(Timestamp value) {
        setCreatedDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CategoriesMasterRecord values(Integer value1, String value2, String value3, Timestamp value4, Timestamp value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached CategoriesMasterRecord
     */
    public CategoriesMasterRecord() {
        super(CategoriesMaster.CATEGORIES_MASTER);
    }

    /**
     * Create a detached, initialised CategoriesMasterRecord
     */
    public CategoriesMasterRecord(Integer id, String catName, String isActive, Timestamp updatedDate, Timestamp createdDate) {
        super(CategoriesMaster.CATEGORIES_MASTER);

        set(0, id);
        set(1, catName);
        set(2, isActive);
        set(3, updatedDate);
        set(4, createdDate);
    }

	@Override
	public Integer component1() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component2() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String component3() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component4() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Timestamp component5() {
		// TODO Auto-generated method stub
		return null;
	}
}
